/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var quest_host = "https://quest.firstvds.ru/";

// Взаимодействие с движком квеста свелось в итоге к одному действию - передачи информации о прохождении игры с проверкой авторизации.
// Авторизацию сделаем по логину и паролю. В качестве меры дополнительной безопасности, можем ограничить по диапазону ip адресов со стороны движка квеста.
var send_codewar_result = function (quest_session, codewar_result, code) {
	if (quest_session) {
		var request = require('request');
		request.post({
			headers: {'content-type': 'application/x-www-form-urlencoded'},
			url: quest_host + 'quest/codewar_api_user_result',
			form: {
				auth: {
					user: "codewaruser",
					password: "password"
				},
				result: codewar_result,
				session: quest_session,
				code: code
			}
		}, function (error, response, body) {
			//console.log(response);
			console.log(body);
		});
	}
}

module.exports.send_codewar_result = send_codewar_result;