/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var express = require('express');
var app = express();
var game = require('./game'); // подключаем механику игры
var quest = require('./quest'); // подключаем интерфейс взаимодействия с движком квеста для проверки авторизации и передачи результатов выполнения задания

var fs = require('fs');
var http = require('http');
var https = require('https');
// подключим ssl сертификат
var privateKey  = fs.readFileSync('/var/www/codewar/ssl/codewar.firstvds.ru.key', 'utf8');
var certificate = fs.readFileSync('/var/www/codewar/ssl/codewar.firstvds.ru.crt', 'utf8');


app.use(function (req, res, next) {
	// разрешим кроссдоменные запросы
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/enviroment', function (req, res) {
	// ответ на запрос начальных данных игры
  var myGame = new game.Game();
  res.send(myGame.enviroment());
});

app.get('/result', function (req, res) {
	// достанем из запроса данные, которые приходят от клиента
  var url = require('url');
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
	// код, который написал игрок
  var code = query.code;
	// идентификатор сессии
  var quest_session = query.session;
	
	// запустим codewar и получим результат в json формате
	var myGame = new game.Game();
  var result = myGame.play(code);
	
	// передадим движку квеста результат прохождения
  quest.send_codewar_result(quest_session, result.result, code);
	
	// отправим результат пользователю
  res.send(result);
});

var credentials = {key: privateKey, cert: certificate};
var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(80);
httpsServer.listen(443);
