function mind(data) {
//vstorage
  var re;
  var vp = data.nexa.position;
  var lp = data.john.position;
  var dis_x = Math.abs(data.nexa.position.x - data.john.position.x);
  var dis_y = Math.abs(data.nexa.position.y - data.john.position.y);
  if (lp.x < vp.x && lp.y < vp.y) {
    if (dis_x > dis_y) {
      re = "left";
    } else {
      re = "up";
    }
  } else if (lp.x >= vp.x && lp.y < vp.y) {
    if (dis_x > dis_y) {
      re = "right";
    } else {
      re = "up";
    }
  } else if (lp.x >= vp.x && lp.y >= vp.y) {
    if (dis_x > dis_y) {
      re = "right";
    } else {
      re = "down";
    }
  } else if (lp.x < vp.x && lp.y >= vp.y) {
    if (dis_x > dis_y) {
      re = "left";
    } else {
      re = "down";
    }
  }
  return re;
}