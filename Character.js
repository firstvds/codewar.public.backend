/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


module.exports = function (members, options, timeline, sleep_steps) {
  this.members = members;
  this.last_action = "";
  this.no_cooldown = false;
	
	// опишем функции движения. Если заявленное объектом движение невозможно, то выполняется действие hold
  this.left = function () {
    if (this.members[this.who].position.x > 1) {
      this.members[this.who].position.x--;
    } else {
      this.hold();
    }
  }

  this.right = function () {
    if (this.members[this.who].position.x <= options.scale.x - 1) {
      this.members[this.who].position.x++;
    } else {
      this.hold();
    }
  }

  this.up = function () {
    if (this.members[this.who].position.y > 1) {
      this.members[this.who].position.y--;
    } else {
      this.hold();
    }
  }

  this.down = function () {
    if (this.members[this.who].position.y <= options.scale.y - 1) {
      this.members[this.who].position.y++;
    } else {
      this.hold();
    }
  }

	//Опишем проверку возможности совершать действие. Используется свойство объекта sleep_steps - количество "пропущенных" ходов
  this.checkActionPossibility = function () {
    var cooldown = options[this.who].cooldown;
    if (!this.no_cooldown && cooldown > 0 && sleep_steps[this.who] < cooldown) {
      return false;
    } else {
      return true;
    }
  }
	
	// функция бездействия
  this.hold = function () {
    this.action = 'hold';
  }
	
	// функция, принимающая строку с названием действия и проверяющая возможность его сотворения
  this.action = function (action) {
    this.action = action;
    if (this.checkActionPossibility()) {
      switch (action) {
        case 'left':
          this.left();
          break;
        case 'right':
          this.right();
          break;
        case 'up':
          this.up();
          break;
        case 'down':
          this.down();
          break;
        default:
					// действие для функций, которые определены у наследуемого объекта (John или Nexa) индивидуально
          if (typeof this.ind_action == "function") {
            this.ind_action(action);
          } else {
            this.hold();
          }
          break
      }
    } else {
      this.hold();
    }

		// Если объект совершает действие hold, количество sleep_steps увеличиваем на 1
    if (this.action == 'hold') {
      sleep_steps[this.who]++;
    } else {
      sleep_steps[this.who] = 0;
    }
    members[this.who].last_action = this.action;
  }
}