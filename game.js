/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Основная механика игры
var Game = function () {
	// определим константы
	// подключаем библиотеку песочницы
	const {VM, VMScript} = require('vm2');

	// подключаем библиотеку для работы с временем
	var now = require("performance-now");
  // подключим библиотеку для работы с файловой системой
	var fs = require('fs');

	// зададим параметры игры
	var options = {
		scale: {// размер сетки
			x: 10,
			y: 10
		},
		maxSteps: 100, // ограничение на количество ходов
		nexa: {// параметры Нексы
			beginPosition: {// начальная позиция
				x: 8,
				y: 8
			},
			cooldown: 3 // задержка ходов
		},
		john: {// параметры Джона
			beginPosition: {
				x: 3,
				y: 3
			},
			cooldown: 0
		},
		infection: [// начальная инфецированная область
			{x: 1, y: 1},
			{x: 5, y: 5},
			{x: 8, y: 2},
			{x: 2, y: 7},
			{x: 4, y: 3},
			{x: 10, y: 8}
		]
	}

	// объявим глобальные переменные игры
	var vm_john, vm_nexa; // переменные для виртаульных машин
	var timeline = []; // массив для полного лога ходов
	var sleep_steps = {nexa: 0, john: 0}; // храним количество ходов простоя для Джона и Нексы
	var result = {// Объект с результатами
		win: false,
		code_length: 0,
		john_time: 0,
		steps: 0,
		moves: 0,
		cured: 0,
		infected: 0,
		version: 1,
		maxSteps: options.maxSteps
	};

	var members = {// информация о текущем состоянии Нексы, Джона и инфецированной области
		john: {},
		nexa: {},
		infection: options.infection
	};

	var finish = false; // переменная, определяющая окончание игры
	var current_step = 0; // Номер текущего хода
	//
	// подключим класс описывающий основное поведение персонажей
	var Character = require('./Character');

	// функция запуска игры
	var play = function (code) {
		var storage = {}; // хранилище информации для Джона
		var vstorage = {}; // хранилище информации для Нексы
		finish = false;

		// создаём виртуальную машину для пользовательского кода игрока
		vm_john = new VM({
			sandbox: {storage},
			timeout: 50
		});

		// создаём виртулальную машину для кода Нексы
		vm_nexa = new VM({
			sandbox: {vstorage},
			timeout: 50
		});


		//session({version: 3});
		sleep_steps = {// обнулим шаги простоя
			nexa: 0,
			john: 0
		}
		result = {// обнулим результаты игры
			win: false,
			code_length: 0,
			john_time: 0,
			steps: 0,
			moves: 0,
			cured: 0,
			infected: 0,
			version: 1,
			maxSteps: options.maxSteps
		};

		result.infected += options.infection.length; // посчитаем стартовое количество зараженных клеток

		timeline = []; // обнулим лог ходов

		// внесем стартовое положение Джона
		var john =
			{
				position: options.john.beginPosition,
				last_action: 'hold'
			}

		// внесем стартовое положение Нексы
		var nexa =
			{
				position: options.nexa.beginPosition,
				last_action: 'hold'
			}

		// определим объект с текущими динамическими данными игры
		var members = {
			john: john,
			nexa: nexa,
			infection: options.infection
		}

		// заразим клетку, где стоит Некса и прибавим
		members.infection.push({x: nexa.position.x, y: nexa.position.y});
		result.infected++;

		timeline[0] = members; // пишем в лог первый ход

		current_step = 1;
		// запускаем основной цикл ходов игры. Выполняется пока не достигнем ограничения или пока игрок не победит.
		while (current_step < 100 && !result.win) {
			// клонируем объект с помощью хака сериализации/десериализации, запускаем функцию обработки хода step и пишем в лог результат:
			timeline[current_step] = step(JSON.parse(JSON.stringify(timeline[current_step - 1])), code);
			current_step++;
		}

		// запускаем подсчет некоторых числовых данных для представления итога игры. Результат пишется в объект result
		calcResultVariables(code);

		// возвращаем объект с логом ходов и результаты игры
		return {
			timeline: timeline,
			result: result
		}
	}

	/*	debug = function (obj) {
	 timeline[current_step].debug = obj;
	 }*/





	// определим функцию, характеризующую персонажа John
	John = function (members) {
		this.who = "john";
		// Воспользумся методом call, чтобы получить в this методы и свойства Character.
		Character.call(this, members, options, timeline, sleep_steps);

		// создаим оболочку для запуска виртуальной машины Джона и обработки её результатов
		this.vm_wrapper = function (code, members) {
			var re = "";
			// сделам конкатинацию введенного кода и кода запуска функции mind. На вход функции подаём состояние динамичной среды members и начальных данных options
			var codew = code + '  mind(' + JSON.stringify(members) + ', ' + JSON.stringify(options) + ');';
			try {
				var script = new VMScript(codew).compile();
			} catch (e) {
				re = "error";
				members.john.error = 'Ошибка ' + e.name + ": " + e.message;
			}
			if (re != "error") {
				try {
					re = vm_john.run(codew);
				} catch (e) {
					re = "error";
					members.john.error = 'Ошибка ' + e.name + ":" + e.message;
				}
			}
			return re;
		}

		// опишем функцию step, запускающую каждый ход Джона. 
		this.step = function (code) {
			// действие по умолчанию hold
			var re = 'hold';
			var john_mind = code;
			var time = now();
			re = this.vm_wrapper(john_mind, members);
			// посчитаем время на выполнение пользовательского кода
			time = now() - time;
			result.john_time += time;
			return re;
		}

		// опишем набор индивидуальных действий, доступных джону. В нашем случае это только действие cure - очистить зараженную клетку
		this.ind_action = function (action) {
			switch (action) {
				case 'cure':
					this.disinfect();
					break;
			}
		}

		// создадим функцию лечения зараженной клетки
		this.disinfect = function () {
			var cell = checkInfectedCell(members.infection, members.john.position.x, members.john.position.y);
			//delete cell;
			if (members.infection.indexOf(cell) != -1) {
				members.infection.splice(members.infection.indexOf(cell), 1);
				result.cured++;
			}
		}
		
		
	}

	// определим функцию, характеризующую персонажа Nexa, аналогично функции John
	Nexa = function (members) {
		this.who = "nexa";
		Character.call(this, members, options, timeline, sleep_steps);

		this.step = function () {
			var re = 'hold';
			if (!sleep_steps.nexa && timeline[current_step - 2] &&
				checkInfectedCell(timeline[current_step - 2].infection, members.nexa.position.x, members.nexa.position.y)
				) {
				this.no_cooldown = true;
			}
			// вытащим скрипт актуальной версии кода Нексы
			var nexa_mind = fs.readFileSync('./vm_scripts/nexa_v3.js', 'utf8');
			re = vm_nexa.run(nexa_mind + '  mind(' + JSON.stringify(members) + ', ' + JSON.stringify(options) + ');');
			return re;
		}

		// опишем набор индивидуальных действий, доступных джону. В нашем случае это только действие infect - заразить клетку
		this.ind_action = function (action) {
			switch (action) {
				case 'infect':
					this.infect();
					break;
			}
		}

		// создадим функцию зарежения клетки
		this.infect = function () {
			result.infected++;
			if (!checkInfectedCell(members.infection, members.nexa.position.x, members.nexa.position.y)) {
				members.infection.push({x: members.nexa.position.x, y: members.nexa.position.y});
			}
		}
	}

	// опишем общую функцию step, запускающую последовательное выполнение шага Нексы и шага Джона
	var step = function (members, user_code) {
		// Некса действует:
		var nexa = new Nexa(members);
		var action = nexa.step();
		if (action == "error") {
			result.error = "Некса тронулась умом и не может ничего делать :-(";
		}
		nexa.action(action);
		// тут привелегия для Нексы: если она наступает на незараженную клетку, то заражает её без затраты хода
		if (nexa.action != 'hold') {
			nexa.infect();
		}

		// Джон действует:
		var john = new John(members);
		var action = john.step(user_code);
		if (action == "error") {
			result.error = "Джон тронулся умом и не может ничего делать :-(";
		}
		john.action(action);

		// ведем счетчик движений Джона
		if (["left", "right", "up", "down"].indexOf(john.action) != -1)
			result.moves++;

		// проверяем условие победы Джонаы
		result.win = !members.infection.length;
		if (result.win)
			finish = true;
		return members;
	}

	// функция, возвращающая стартовых параметров игры
	var enviroment = function () {
		return JSON.parse(JSON.stringify(options));
	}

	// функция, записывающая в result длинну кода и количество шагов
	var calcResultVariables = function (code) {
		var min_code = String(code).replace(/[\s]^ /g, '');
		min_code = min_code.replace(/\s+/g, ' ');
		result.code_length = min_code.length;
		result.steps = timeline.length;
	}

	// функция проверки клетки поля на инфецированность
	var checkInfectedCell = function (infection, x, y) {
		if (infection) {
			for (var k in infection) {
				if (infection[k].x == x && infection[k].y == y) {
					return infection[k];
				}
			}
		}
		return false;
	}

	this.play = play;
	this.enviroment = enviroment;
}
module.exports.Game = Game;
